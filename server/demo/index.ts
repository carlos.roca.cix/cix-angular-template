((module) => {
  const express = require('express');
  const app = express();
  const timeoutResponse = 1000;

  app.get('/security/login', (req: any, res: any) => {
    const data = require('./data/demo.json');
    setTimeout(() => {
      return res.status(200).json(data);
    }, timeoutResponse);
  })

  module.exports = app;
})(module);
