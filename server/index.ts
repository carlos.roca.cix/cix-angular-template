(() => {
  const express = require('express');
  const bodyParser = require('body-parser');
  const app = express();

  const demoModule = require('./demo');

  const demoPrefix = '/channel/demo/v1';

  const PORT = 6969;

  app.use(bodyParser.json({ limit: '5mb' }));
  app.use((req: any, res: any, next: any) => {
    console.log(req.url);
    const allowedOrigins = ['http://127.0.0.1:4200', 'http://localhost:4200'];
    const origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
      res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Headers', 'Content-type, Authorization, x-xsrf-token');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, PATCH');
    next();
  });

  app.all('*', (req: any, res: any, next: any) => setTimeout(() => next(), 0));

  app.use(demoPrefix, demoModule);

  app.get('/', (req: any, res: any) => {
    res.send({
      title: 'Proyecto Base (frontend)'
    });
  });

  app.listen(PORT, () => console.log(`Server listening ` + PORT))
    .on('error', (err: any) => {
      console.error(err);
    });
})();
