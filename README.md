# Proyecto Base en Angular - CIX

## Instalacion
Instalar dependencias, devDependencias y levantar proyecto.

```sh
npm install
npm run start
```
Levantar **mock server**
```sh
npm run start:mock
```

## Correr Sonarqube
#### Requisitos
- Instalar Docker
- Ejecutar
```sh
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```
- Ingresar a http://localhost:9000 e ingresar con username:admin y password:admin
- Ingresar a 'Administration', luego 'Security'
- Deshabilitar 'Force user authentication'
- Crear el archivo 'sonar-project.properties' con el siguiente bloque de codigo a la raiz del proyecto
```
sonar.host.url=http://localhost:9000
sonar.login=admin
sonar.password=admin
sonar.projectKey=demo-web
sonar.projectName=demo-web
sonar.projectVersion=1.0
sonar.sourceEncoding=UTF-8
sonar.sources=src
sonar.tests=src
sonar.test.inclusions=**/*.spec.ts
sonar.typescript.lcov.reportPaths=coverage/lcov.info
```
#### Ejecutar sonar
Ejecutar el siguiente comando
```sh
npm run sonar
```

## Ejecutar Coverage
Ejecutar el siguiente comando
```sh
npm run coverage
```
