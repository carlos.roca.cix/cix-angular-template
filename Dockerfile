# Imagen base
FROM nginx:latest
# Librerias requeridas
RUN apt-get update
RUN apt-get install -y nginx-extras

# Eliminar el archivo de configuracion de NGINX por defecto
RUN rm -v /etc/nginx/conf.d/default.conf
RUN rm -v /etc/nginx/nginx.conf

# Copiar el bundle generado a la ruta publica del NGINX
ADD ./dist/comu-web /var/www/code

# Copiar el archivo de configuracion de NGINX
ADD ./devops/docker/nginx.webapp.conf /etc/nginx/conf.d/nginx_tmp.conf
ADD ./devops/docker/nginx.root.conf /etc/nginx/nginx.conf
ADD ./devops/docker/webapp_startup.sh /usr/bin/webapp_startup.sh

RUN chmod +x /usr/bin/webapp_startup.sh
# Ejecutar el comando envsubst para reemplazar las variables de entorno en el archivo de configuracion del NGINX
# Iniciar el servicio de NGINX
CMD ["webapp_startup.sh"]

# Exponer el puerto especificado en la variable de entorno $WEB_APP_PORT
EXPOSE 80
