import { Component, OnInit } from '@angular/core';
import { DemoService } from 'src/app/core/services/DEMO.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private demoService: DemoService,
  ) { }

  ngOnInit(): void {
    this.getDemoData();
  }

  getDemoData(): void {
    this.demoService.demo().subscribe((response: any) => {
      console.log(response)
    });
  }

}
