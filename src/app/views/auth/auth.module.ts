import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { LoginComponent } from '../auth/login/login.component';
import { RegisterComponent } from '../auth/register/register.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ROUTES } from 'src/app/core/models/routers';

const routes: Routes = [
  {
    path: ROUTES.AUTH.ROUTES.LOGIN.PATH,
    component: LoginComponent
  },
  {
    path: ROUTES.AUTH.ROUTES.REGISTER.PATH,
    component: RegisterComponent
  },
  {
    path: '',
    redirectTo: ROUTES.AUTH.ROUTES.LOGIN.PATH
  },
  {
    path: '**',
    redirectTo: ROUTES.AUTH.ROUTES.LOGIN.PATH
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
})
export class AuthModule { }
