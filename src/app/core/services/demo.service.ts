import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DemoService {

  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) { }

  demo(): Observable<any> {
    return this.http.get(this.apiService.endpoints.apiDemo.login());
  }
}
