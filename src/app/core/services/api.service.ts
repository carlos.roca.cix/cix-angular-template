import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Api, protocolRegExp } from '../models/api';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public endpoints: Api = new Api();
  private host: string;
  private apiPrefix: string;
  private apiVersion: string;

  constructor() {
    this.host = environment.backend.host;
    this.apiPrefix = 'channel';
    this.apiVersion = '1';
  }

  public createUrl(url: string, prefix = 'demo'): string {
    const protocolValidation = new RegExp(protocolRegExp.httpOrHttps);

    if (protocolValidation.test(url) || this.isAssetResource(url)) {
      return url;
    }

    return `${this.host}/${this.apiPrefix}/${prefix}/v${this.apiVersion}${url}`;
  }

  public isAssetResource(url: string) {
    return url.startsWith('/assets');
  }

  public JSONtoParams(jsonObj: any) {
    return Object.keys(jsonObj).map(key => key + '=' + jsonObj[key]).join('&');
  }
}
