export interface ApiDemo {
  login: () => string;
}

export class Api {
  public apiDemo: ApiDemo = {
    login: () => `/security/login`
  };
}

export const protocolRegExp = {
  httpOrHttps: '^(http|https)://'
};

export const statusCode = {
  bad_request: 400,
  unauthorized: 401,
  forbidden: 403,
  not_found: 404,
  internal_server_error: 500
};

export enum DEMOCode {
  INVALID_USER_PASSWORD = 'DEMO-03109',
  CLIENT_USER_NOT_EXIST = 'DEMO-09876',
  CLIENT_USER_ALREADY_EXIST = 'DEMO-08787',
  OTP_WRONG_PIN = 'DEMO-05654'
}
