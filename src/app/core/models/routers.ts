export const ROUTES = {
  INDEX: {
    PATH: '/',
  },
  HOME: {
    PATH: 'home',
    PAGE_NAME: 'home'
  },
  AUTH: {
    PATH: 'auth',
    ROUTES: {
      LOGIN: {
        PATH: 'login',
        FULL_PATH: 'auth/login',
        PAGE_NAME: 'login'
      },
      REGISTER: {
        PATH: 'register',
        FULL_PATH: 'auth/register',
        PAGE_NAME: 'register'
      }
    }
  },
  SERVER_ERROR: {
    PATH: '500',
    PAGE_NAME: 'Error'
  }
};
