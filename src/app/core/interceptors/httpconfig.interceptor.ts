import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { catchError, finalize } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';
import { DEMOCode, statusCode } from '../models/api';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  constructor(
    private apiService: ApiService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.apiService.isAssetResource(req.url)) {
      return next.handle(req);
    }

    const request = req.clone({
      url: this.apiService.createUrl(req.url)
    });

    return next.handle(request).pipe(
      catchError(error => {
        const errorCode = error && error.error && error.error.code;

        if (error instanceof HttpErrorResponse && errorCode) {
          switch (error.status) {
            case statusCode.bad_request:
              this.handle400Error(request, next, errorCode);
              break;
            case statusCode.unauthorized:
              this.handle401Error(request, next, errorCode);
              break;
            case statusCode.not_found:
              this.handle404Error(request, next, errorCode);
              break;
            case statusCode.internal_server_error:
              this.handle500Error(request, next, errorCode);
              break;
            default:
              // show general error
          }
        } else {
          // show general error
        }

        return throwError(error);
      }),
      finalize(() => {
        // hide loader
      })
    );
  }

  private handle400Error(request: HttpRequest<any>, next: HttpHandler, errorCode: string) {
    switch (errorCode) {
      case DEMOCode.OTP_WRONG_PIN:
        break;
      default:
        // show general error
    }
    return next.handle(request);
  }

  private handle404Error(request: HttpRequest<any>, next: HttpHandler, errorCode: string) {
    switch (errorCode) {
      case DEMOCode.CLIENT_USER_NOT_EXIST:
        break;
      default:
        // show general error
    }
    return next.handle(request);
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler, errorCode: string) {
    if (errorCode !== DEMOCode.INVALID_USER_PASSWORD) {
      // add jwt service

    }
    return next.handle(request);
  }

  private handle500Error(request: HttpRequest<any>, next: HttpHandler, errorCode: string) {
    switch (errorCode) {
      case DEMOCode.CLIENT_USER_ALREADY_EXIST:
        break;
      default:
        // show general error
    }
    return next.handle(request);
  }
}
