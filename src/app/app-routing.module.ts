import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ROUTES } from './core/models/routers';
import { ServerErrorComponent } from './shared/component/server-error/server-error.component';
import { HomeComponent } from './views/home/home.component';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  {
    path: ROUTES.AUTH.PATH,
    loadChildren: () => import('./views/auth/auth.module').then(m => m.AuthModule),
    canActivate: [AuthGuard],
  },
  {
    path: '500',
    component: ServerErrorComponent
  },
  {
    path: ROUTES.HOME.PATH,
    component: HomeComponent
  },
  {
    path: '',
    redirectTo: ROUTES.HOME.PATH,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: ROUTES.HOME.PATH,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
