#!/bin/sh
envsubst '\${AKS_INGRESS_URL} \${APIM_SUBSCRIPTION_KEY}' < /etc/nginx/conf.d/nginx_tmp.conf > /etc/nginx/conf.d/default.conf
rm -v /etc/nginx/conf.d/nginx_tmp.conf
exec nginx -g 'daemon off;'
