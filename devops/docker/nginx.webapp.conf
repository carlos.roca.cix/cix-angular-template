server {
  listen       80;
  server_name  localhost;
  charset      utf-8;
  tcp_nopush   on;
  tcp_nodelay  on;
  server_tokens off;

  more_clear_headers "Server: ";
  more_clear_headers "Last-Modified: ";
  more_clear_headers "Via: ";

  client_max_body_size 4M;
  add_header Strict-Transport-Security "max-age=31536000;" always;
  # add custom Content-Security-Policy based on your project
  add_header Content-Security-Policy "default-src 'self' https://${AKS_INGRESS_URL}; script-src 'self' https://${AKS_INGRESS_URL} 'unsafe-inline' 'unsafe-eval' https://api.segment.io https://cdn.segment.com https://optimize.google.com https://www.googleoptimize.com https://www.googletagmanager.com https://www.google-analytics.com https://connect.facebook.net https://www.google.com https://www.gstatic.com https://detectca.easysol.net http://detectca.easysol.net https://maps.googleapis.com https://js-cdn.dynatrace.com https://static.hotjar.com https://script.hotjar.com *.fullstory.com https://widget.intercom.io https://assets.customer.io https://cdn.amplitude.com https://js.intercomcdn.com https://d2yyd1h5u9mauk.cloudfront.net; img-src 'self' blob: data: https://downloads.intercomcdn.com https://track.customer.io https://static.intercomassets.com  https://static.hotjar.com https://www.fullstory.com https://widget.intercom.io https://js.intercomcdn.com https://geo0.ggpht.com/cbk https://cbks0.googleapis.com/cbk https://www.google-analytics.com https://www.google.com https://www.facebook.com https://detectca.easysol.net http://detectca.easysol.net https://www.gstatic.com https://maps.googleapis.com https://maps.gstatic.com https://stats.g.doubleclick.net https://www.googletagmanager.com http://www.googletagmanager.com https://www.googleoptimize.com https://optimize.google.com https://script.hotjar.com https://www.google.com.pe data:; style-src 'self' 'unsafe-inline' https://www.gstatic.com https://optimize.google.com https://www.googleoptimize.com https://fonts.googleapis.com; font-src 'self' https://static-bcp.azureedge.net https://fonts.gstatic.com https://script.hotjar.com https://js.intercomcdn.com data:; frame-src https://www.youtube.com/ https://hogaremi.typeform.com https://vars.hotjar.com https://www.google.com https://maps.googleapis.com https://optimize.google.com https://8739215.fls.doubleclick.net/ https://www.googletagmanager.com/; object-src 'none'; connect-src 'self' data: https://${AKS_INGRESS_URL} https://stats.g.doubleclick.net https://optimize.google.com https://www.google-analytics.com https://api.segment.io https://cdn.segment.com http://api.amplitude.com https://api-iam.intercom.io https://in.hotjar.com https://vc.hotjar.io https://rs.fullstory.com https://*.hotjar.com wss://*.hotjar.com https://web.delighted.com wss://ws.pusherapp.com wss://nexus-websocket-a.intercom.io";
  add_header X-Frame-Options "SAMEORIGIN" always;

  add_header X-XSS-Protection "1; mode=block" always;
  add_header X-Content-Type-Options "nosniff" always;
  add_header Referrer-Policy "no-referrer-when-downgrade";
  add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; camera 'none'; encrypted-media 'none'; fullscreen 'self'; geolocation 'self'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; speaker 'self'; sync-xhr 'self'; usb 'none'; payment 'none'; vr 'none';";
  add_header Set-Cookie "HttpOnly; Secure" always;
  add_header Cache-Control "no-cache, no-store, must-revalidate" always;
  add_header Pragma "no-cache";
  gzip on;
  gzip_disable "msie6";
  gzip_vary on;
  gzip_proxied any;
	gzip_comp_level 6;
	gzip_buffers 16 8k;
  gzip_http_version 1.0;
  gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript image/jpeg image/png image/svg+xml font/woff;

  # redirect server_error's
  error_page 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 420 422 423 424 426 428 429 431 444 449 490 491 492 493 494 495 496 497 450 451 500 501 502 503 504 505 506 507 508 509 510 511 /server_error.html;
    location = /server_error.html {
      root   /var/www/code/assets/html;
      allow all;
  }

  ## Block SQL injections
  set $block_sql_injections 0;
  if ($query_string ~ ".*") {
    set $block_sql_injections A;
  }
  # if ($query_string ~ "(.*)\=.*") {
  #   set $block_sql_injections "${block_sql_injections}B";
  # }
  if ($query_string ~ "(.*page|.*returnUrl)\=.*") {
    set $block_sql_injections "${block_sql_injections}C";
  }
  if ($query_string ~ ".*select.*from.*") {
    set $block_sql_injections 1;
  }
  if ($query_string ~ ".*union.*select.*\(") {
    set $block_sql_injections 1;
  }
  if ($query_string ~ "union.*all.*select.*") {
    set $block_sql_injections 1;
  }
  if ($query_string ~ "concat.*\(") {
    set $block_sql_injections 1;
  }

  if ($block_sql_injections = AB) {
    return 403;
  }
  if ($block_sql_injections = 1) {
    return 403;
  }

  ## Block file injections
  set $block_file_injections 0;
  if ($query_string ~ "[a-zA-Z0-9_]=http://") {
    set $block_file_injections 1;
  }
  # if ($query_string ~ "[a-zA-Z0-9_]=(\.\.//?)+") {
  #   set $block_file_injections 1;
  # }
  # if ($query_string ~ "[a-zA-Z0-9_]=/([a-z0-9_.]//?)+") {
  #   set $block_file_injections 1;
  # }
  if ($block_file_injections = 1) {
    return 403;
  }

  ## Block common exploits
  set $block_common_exploits 0;
  if ($query_string ~ "GLOBALS(=|\[|\%[0-9A-Z]{0,2})") {
    set $block_common_exploits 1;
  }
  if ($query_string ~ "_REQUEST(=|\[|\%[0-9A-Z]{0,2})") {
    set $block_common_exploits 1;
  }
  if ($query_string ~ "proc/self/environ") {
    set $block_common_exploits 1;
  }
  if ($query_string ~ "mosConfig_[a-zA-Z_]{1,21}(=|\%3D)") {
    set $block_common_exploits 1;
  }
  if ($query_string ~ "base64_(en|de)code\(.*\)") {
    set $block_common_exploits 1;
  }
  if ($block_common_exploits = 1) {
    return 403;
  }

  ## Block spam
  set $block_spam 0;
  if ($query_string ~ "\b(javascript|query|where)\b") {
    set $block_spam 1;
  }
  if ($block_spam = 1) {
    return 403;
  }

  ## Block user agents
  set $block_user_agents 0;

  if ($http_user_agent ~ "Wget") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "Indy Library") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "libwww-perl") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "GetRight") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "GetWeb!") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "Go!Zilla") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "Download Demon") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "Go-Ahead-Got-It") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "TurnitinBot") {
    set $block_user_agents 1;
  }
  if ($http_user_agent ~ "GrabNet") {
    set $block_user_agents 1;
  }

  if ($block_user_agents = 1) {
    return 403;
  }

  location ~* "(eval\()"  { deny all; }
  location ~* "(127\.0\.0\.1)"  { deny all; }
  location ~* "([a-z0-9]{2000})"  { deny all; }
  location ~* "(javascript\:)(.*)(\;)"  { deny all; }
  location ~* "(base64_encode)(.*)(\()"  { deny all; }
  location ~* "(GLOBALS|REQUEST)(=|\[|%)"  { deny all; }
  location ~* "(<|%3C).*script.*(>|%3)" { deny all; }
  location ~ "(\\|\.\.\.|\.\./|~|`|<|>|\|)" { deny all; }
  location ~* "(boot\.ini|etc/passwd|self/environ)" { deny all; }
  location ~* "(thumbs?(_editor|open)?|tim(thumb)?)\.php" { deny all; }
  location ~* "(\'|\")(.*)(drop|insert|md5|select|union)" { deny all; }
  location ~* "(https?|ftp|php):/" { deny all; }
  location ~* "(=\\\'|=\\%27|/\\\'/?)\." { deny all; }
  location ~* "/(\$(\&)?|\*|\"|\.|,|&|&amp;?)/?$" { deny all; }
  location ~ "(\{0\}|\(/\(|\.\.\.|\+\+\+|\\\"\\\")" { deny all; }
  location ~ "(~|`|<|>|:|;|%|\\|\s|\{|\}|\[|\]|\|)" { deny all; }
  location ~* "/(=|\$&|_mm|(wp-)?config\.|cgi-|etc/passwd|muieblack)" { deny all; }
  location ~* "(&pws=0|_vti_|\(null\)|\{\$itemURL\}|echo(.*)kae|etc/passwd|eval\(|self/environ)" { deny all; }
  location ~* "\.(aspx?|bash|bak?|cfg|cgi|dll|exe|git|hg|ini|jsp|log|mdb|out|sql|svn|swp|tar|rdf)$" { deny all; }
  location ~* "/(^$|mobiquo|phpinfo|shell|sqlpatch|thumb|thumb_editor|thumbopen|timthumb|webshell)\.php" { deny all; }

  location / {
    root   /var/www/code;
    index  index.html index.htm;
    try_files $uri /index.html;
  }

  # add custom paths based on your project
  # Example

  # location /channel/comu/v1/ {
  #   client_max_body_size 4M;
  #   proxy_set_header X-Forwarded-Host $host;
  #   proxy_set_header X-Forwarded-Server $host;
  #   proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  #   proxy_set_header Ocp-Apim-Subscription-Key ${APIM_SUBSCRIPTION_KEY};

  #   proxy_pass https://${AKS_INGRESS_URL}/cicm/channel/comu/community/v1/;
  # }

}
