@Library('cix-shared-library')
import sharedlib.JenkinsfileUtil

def utils = new JenkinsfileUtil(steps, this)

def deploymentEnviroment = "prod"
def projectName = 'comu' // 4-digit project code
def appName = 'comu-web' // projectName-web
def spAppId = 'SVPRAPPCOMUPRO01'
def resourceGroupName = 'rsgreu2comup01'
def wapName = 'wapceu2comup01'
def aksIngressUrl = 'apim.innovacxion.com/comu'

node('master')
{
  stage('Preparation')
  {
    env.deploymentEnviroment = "${deploymentEnviroment}"
    env.projectName = "${projectName}"
    env.appName = "${appName}"

    utils.prepare()
  }

  stage('SCM')
  {
    checkout scm
  }

  stage('Build')
  {
    utils.buildAngularApp("${appName}")
  }

  stage('Docker Build')
  {
    utils.buildDockerImage();
  }

  stage('Deploy')
  {
    utils.deployImageToWebApp("${spAppId}", "${resourceGroupName}", "${wapName}", "${aksIngressUrl}")
  }
}
