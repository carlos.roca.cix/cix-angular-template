@Library('cix-shared-library')
import sharedlib.JenkinsfileUtil

def utils = new JenkinsfileUtil(steps, this)

def deploymentEnviroment = "dev"
def projectName = 'comu' // 4-digit project code
def appName = 'comu-web' // projectName-web
def spAppId = 'SVPRAPPCOMUDES01'
def resourceGroupName = 'rsgreu2comud01'
def wapName = 'wapceu2comud01'
def aksIngressUrl = 'apimeu2recod01.azure-api.net/comu'

node('master')
{
  stage('Preparation')
  {
    env.deploymentEnviroment = "${deploymentEnviroment}"
    env.projectName = "${projectName}"
    env.appName = "${appName}"

    utils.prepare()
  }

  stage('SCM')
  {
    checkout scm
  }

  stage('Build')
  {
    utils.buildAngularApp("${appName}")
  }

  stage('SonarQube Analysis')
  {
    utils.executeSonarForTypeScript("${appName}")
  }

  stage('SonarQube Quality Validation')
  {
    utils.validateSonarQualityGate()
  }

  stage('Docker Build')
  {
    utils.buildDockerImage()
  }

  stage('Deploy')
  {
    utils.deployImageToWebApp("${spAppId}", "${resourceGroupName}", "${wapName}", "${aksIngressUrl}")
  }
}
